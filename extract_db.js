var http = require('http');
var https = require('https');
var cmd = require('node-cmd');
var fs = require('fs');
var rp = require('request-promise');
var PouchDB = require('pouchdb');
var _ = require('lodash');
var spawnPouchdbServer = require('spawn-pouchdb-server')
var AWS = require('aws-sdk');
const couchbackup = require('@cloudant/couchbackup');
PouchDB.plugin(require('pouchdb-load'));

var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport({
	service: 'gmail',
	auth: {
		user: 'testvdragon@gmail.com',
		pass: 'testvdragon!'
	}
});

var main_param_username = process.argv[2].toString();
var main_param_password = process.argv[3].toString();
var main_param_dbname = process.argv[4].toString();
var clinicCode = process.argv[5].toString();
var type = process.argv[6].toString();
var portNumber = process.argv[7].toString();

var body_request ='',
body='',
body_mydb='',
body_source='',
id,
id_string,
json_source;

function decryptAndReplace(cmdCommand_decrypt, backupFileName_dec){
	return new Promise((resolve, reject) => {
		
		cmd.get(cmdCommand_decrypt,function(err,data,stderr){
			if(!err){
				console.log('decryption complete');
				cmd.get('/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/replacescript.sh ' + backupFileName_dec,function(err,data,stderr){
					console.log(err);
					console.log('called replaceScript');
					return resolve();
				});
				
			}
			else{
				return reject();
			}
		});
		
	});
}

function performRestore(isChua){
	
	return new Promise ((resolve,reject) => {
		var restoreDest;
		if(isChua){
		restoreDest = 'https://andrournsianytotiongster:45d58c7830ec7d7f9db4a86aadaf7c331a9ada06@vaultdragon.cloudant.com/test_dr_selina_chua_backup_restore';
		}
		else{
		restoreDest = 'http://localhost:'+portNumber+'/'+param_dbname;
		}
		couchbackup.restore(
			fs.createReadStream('/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt'),
			restoreDest,
			function(err, data) {
				if (err) {
					console.error("Perform Restore Failed! " + err);
					return reject();
				} else {
					console.error("Perform Restore Success! " + data);
					return resolve();
				}
			});
		});
	}
	
	function restore(cmdCommand_pouch, backupFileName, clinicCode, isDragonFly){
		return new Promise((resolve,reject) => {
			console.log('in restore()');
			var bool = (clinicCode === '1d44511ddaabd3bc0ce93e34632daf19') && isDragonFly;
			if(bool){
				console.log('restoring chua');
				return performRestore(true).
				then(function(result){
					console.log('chua restored');
					return resolve();
				},function(rejectResult){
					console.log('chua not restored');
					return reject();
				});
			}
			else {
			console.log('reached here');
			var dbUrl = 'http://localhost:'+portNumber+'/'+ param_dbname;
			console.log(dbUrl);
			var remote_db = new PouchDB(dbUrl);
			console.log('created db');
			
			// setTimeout(function(){console.log('done')},20000);
			var promise_ = new Promise ((resolve, reject) => {
				remote_db.info().then(function (info) {
					console.log('db added');
					resolve();
				});
			}).then(function(result){
				
				var url = 'http://localhost:'+portNumber+'/';
				console.log('reaching here too');
				//var cmdCommand_restore = './testscript.sh '+backupFileName+' '+url+' '+param_dbname;
				var cmdCommand_restore = 'cat '+backupFileName+' | '+'couchrestore --url '+url+' --db '+param_dbname;
				// cmd.get(cmdCommand_restore,function(err,data,stderr){
				// 	if(!err){
				// 		console.log('restore complete');
				// 		return resolve();
				// 	}
				// 	else{
				// 		console.log('rejecting here...');
				// 		console.log(err);
				// 		return reject();
				// 	}
				// });
				return performRestore(false).
				then(function(result){
					return resolve();
				},function(rejectResult){
					return reject();
				})
			})
			}		
		});
	}
	
	function test(clinicCode,isDragonFly){
		return new Promise ((resolve,reject) => {
			
			var request = rp("https://"+param_username+":"+param_password+"@vaultdragon.cloudant.com/"+param_dbname+"/_changes?descending=true&limit=1").
			then(function(chunk){
				var object = JSON.parse(chunk);
				id = object.results[0].id;
				console.log(id);
			},
			function(rejectResult){
				return reject();
			}).then(function(){
				id_string = id.toString();
				
				var request_source = rp("https://"+param_username+":"+param_password+"@vaultdragon.cloudant.com/"+param_dbname+"/"+ id_string);
				
				request_source.then(function(chunk){
					
					json_source = JSON.parse(chunk);
					
				},function(rejectResult){
					console.log(rejectResult);
					return reject();
				}).then(function(){
					var command;
					if(clinicCode === '1d44511ddaabd3bc0ce93e34632daf19' && isDragonFly) {
					command = 'curl -X GET https://andrournsianytotiongster:45d58c7830ec7d7f9db4a86aadaf7c331a9ada06@vaultdragon.cloudant.com/test_dr_selina_chua_backup_restore/'+id_string;
					}
					else {
					command = 'curl -X GET http://localhost:'+portNumber+'/'+param_dbname+'/'+id_string;
					console.log('curl-ing localhost'); 
					}
					cmd.get(command,function(err, data, stderr){
						if(err){
							return reject();
						}
						body_mydb=data.toString();
						body_mydb = body_mydb.trim();
						var json_local = JSON.parse(body_mydb);
						// console.log('json_local:\n');
						// console.log(json_local);
						// console.log('json_source:\n');
						// console.log(json_source);
						fileName = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/encrypted_'+param_dbname+'.txt';
						// console.log(JSON.)
						// if(JSON.stringify(json_local) === JSON.stringify(json_source)){
						//     console.log('Verified dbs');
						//     return resolve();
						
						// }  
						if(difference(json_source,json_local)){
							// console.log('verified');
							return resolve();
						}
						else{
							// console.log('verification failed');
							return reject();
						}
					});
				});
			});
		});
	}
	
	function upload(){
		return new Promise ((resolve,reject) => {
			
			var d = new Date();
			var day,
			week,
			month,
			hour,
			minute;
			switch(d.getDay()){
				case 0: day = 'Sunday'; 
				break;
				case 1: day = 'Monday'; 
				break;
				case 2: day = 'Tuesday'; 
				break;
				case 3: day = 'Wednesday'; 
				break;
				case 4: day = 'Thursday'; 
				break;
				case 5: day = 'Friday'; 
				break;
				case 6: day = 'Saturday'; 
				break; 
			}
			switch(d.getMonth()){
				case 0: month = 'Jan'; 
				break;
				case 1: month = 'Feb'; 
				break;
				case 2: month = 'Mar'; 
				break;
				case 3: month = 'Apr'; 
				break;
				case 4: month = 'May'; 
				break;
				case 5: month = 'Jun'; 
				break;
				case 6: month = 'Jul'; 
				break;
				case 7: month = 'Aug'; 
				break;
				case 8: month = 'Sep'; 
				break;
				case 9: month = 'Oct'; 
				break;
				case 10: month = 'Nov'; 
				break;
				case 11: month = 'Dec'; 
				break;
			}
			week = Math.ceil(d.getDate()/7);
			hour = d.getHours() + 8;			// gmt -> sgt
			var key;
			if((hour<7 || hour>19)){
				console.log('here..');
				if(d.getDate() === 1) {			//monthly backup
					if(d.getMonth()+1<10){
						key = param_dbname+'/'+'m-0'+(d.getMonth()+1)+'-'+month+'/'+param_dbname;
					}
					else {
						key = param_dbname+'/'+'m-'+(d.getMonth()+1)+'-'+month+'/'+param_dbname;
					}
					console.log('monthly');
				}
				if(d.getDay() === 0) {			//weekly backup
					key = param_dbname+'/'+'week-'+week+'/'+param_dbname;	
					console.log('weekly');
				}
				if(hour === 21) {
					key = param_dbname+'/'+'d-'+(d.getDay()+1)+'-'+day+'/'+param_dbname;
					console.log('daily');
				}
			}
			else {						//hourly backup	
				console.log('hourly');
				if(hour<10){
					key = param_dbname+'/'+'h-0'+hour+'/'+param_dbname;
				}
				else {
					key = param_dbname+'/'+'h-'+hour+'/'+param_dbname;
				}
			}

			var bucketName = 'vd-cms-backup';
			// var key = param_dbname+'/'+month+'/'+day+'/'+hour+'/'+param_dbname;
			var param = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/encrypted_'+param_dbname+'.txt';
			console.log(key);
			
			var promise = new Promise ((resolve,reject) => {
				AWS.config.update({ accessKeyId: 'AKIAJRZBGKXVR2IIOENA', secretAccessKey: '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' });
				fs.readFile(param, function(err, data){
					if(err){}
					else{
						var s3 = new AWS.S3();
						s3.putObject({
							Bucket: bucketName,
							Key: key,
							Body: data
						}, function(err, data){ 
							if(err){
								// throw err;
								console.log('failed');
								return reject();
							}
							else{
								// console.log('uploaded.');
								return resolve();
							}
						});
					}
				})
			}).then(function(result){
				return resolve();
			},function (rejectResult){
				return reject();
			})
		});
	}
	
	/**
	* Deep diff between two object, using lodash
	* @param  {Object} object Object compared
	* @param  {Object} base   Object to compare with
	* @return {Object}        Return a new object who represent the diff
	*/
	function difference(object, base) {
		function changes(object, base) {
			return _.transform(object, function(result, value, key) {
				if (!_.isEqual(value, base[key])) {
					result[key] = (_.isObject(value) && _.isObject(base[key])) ? changes(value, base[key]) : value;
				}
			});
		}
		return changes(object, base);
	}
	
	function sendNotification(operation){
		return new Promise ((resolve,reject) => {
			var mailOptions = {
				from: 'testvdragon@gmail.com',
				to: 'yashchowdhary98@gmail.com',
				subject: operation+ ' failed:' + param_dbname,
				text: operation+' of database \''+param_dbname+'\' failed',
			};
			
			transporter.sendMail(mailOptions, function(error, info){
				if (error) {
					console.log(error);
					return reject();
				} else {
					console.log('Email sent: ' + info.response);
					console.log(operation+' failed');
					return resolve();
				}
			}); 
		});
	}
	
	function backup(){
		
		return new Promise ((resolve,reject) => {
			couchbackup.backup('https://'+param_username+':'+param_password+'@vaultdragon.cloudant.com/'+param_dbname,
			fs.createWriteStream(param_dbname+'.txt'),
			{ mode : "shallow"},
			function() {
				console.log('backup complete');
				return resolve();
			});
		});
		
	}
	
	var body ='';
	var param_dbname ='',
	param_username='',
	param_password='';
	
	// var promise = new Promise ((resolve,reject) => {
	//     console.log(portNumber);
	//     // resolve();
	
	//     spawnPouchdbServer({
	//         port: parseInt( portNumber,10)
	//     },function(error, server){
	//         console.log('server created at port '+portNumber);
	//         // server.stop(function () {
	//         //     console.log('PouchDB Server stopped')
	//         // })
	//         return resolve();
	
	//     });
	// })
	// .then(function(result){
	
	spawnPouchdbServer({
		port: parseInt( portNumber,10)
	},function(error, server){
		console.log('server created at port '+portNumber);
		callPromise().then(function(result){
			server.stop(function () {
				console.log('PouchDB Server stopped');
			});
		})        
	});
	
	function callPromise() {
		return new Promise ((resolve,reject) => {
			var request = rp("https://"+main_param_username+":"+main_param_password+"@vaultdragon.cloudant.com/"+main_param_dbname+"/_all_docs?include_docs=true")
			.then(
				function(chunk){
					var object = JSON.parse(chunk.toString());
					var objectToBackup = _.filter(object.rows,function(o){
						return o.id === clinicCode;
					});
					if(type === 'D' && objectToBackup[0].doc.hasOwnProperty('dragonfly')){
						
						param_dbname = objectToBackup[0].doc.dragonfly.cloudantDatabase.toString();
						param_password = objectToBackup[0].doc.dragonfly.cloudantPassword.toString();
						param_username = objectToBackup[0].doc.dragonfly.cloudantUsername.toString();
						
						var dragonflyBackupFileName = 'encrypted_'+param_dbname+'.txt';
						
						var dragonfly_cmdCommand_backup = 'couchbackup --url https://'+ param_username + ':' + param_password
						+ '@vaultdragon.cloudant.com/ --db ' + param_dbname + ' --buffer-size 100 | openssl aes-128-cbc -pass pass:vaultdragon' + ' > ' + dragonflyBackupFileName;
						
						console.log(dragonfly_cmdCommand_backup);
						
						var promise = new Promise (function(resolve,reject){
							
							var backupFileName_enc = 'encrypted_'+param_dbname+'.txt';
							var cmdCommand_encrypt = 'openssl aes-128-cbc -e -in '+param_dbname+'.txt '+' -pass pass:vaultdragon -out '+backupFileName_enc;
							
							return resolve();
						/*	
							return backup().
							then(function(result){
								cmd.get(cmdCommand_encrypt,function(err,data,stderr){
									if(!err){
										console.log('encryption complete');
										return resolve();
									}
									else{
										return reject();
									}
								})
							});
						*/	
							// cmd.get(dragonfly_cmdCommand_backup,function(err,data,stderr){                              // complete backup
							// 	console.log(err);
							// 	if(!err){
							// 		console.log('backup complete');
							// 		return resolve();                                                            
							// 	}    
							// 	else{
							// 		return reject();
							// 	}
							// });
							
						}).then(
							function(backupResult){                                                                     // complete decrypt + replace
								
								var backupFileName_enc = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/encrypted_'+param_dbname+'.txt';
								var backupFileName_dec = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt';
								var cmdCommand_decrypt = 'openssl aes-128-cbc -d -in '+backupFileName_enc +' -pass pass:vaultdragon -out '+backupFileName_dec;
								console.log(cmdCommand_decrypt);
								
								return decryptAndReplace(cmdCommand_decrypt, backupFileName_dec);
								
							},function(rejectResult){
								
								var operation = 'backup/encrypt';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							}).then(function(result){                                                                    // complete restore
								
								var backupFileName = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt';
								var cmdCommand_pouch = 'pouchdb-server --port '+portNumber;
								
								return restore(cmdCommand_pouch, backupFileName,clinicCode, true);
								
							},function(rejectResult){
								
								var operation = 'Decrypt/Replace';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
								
							}).then(function(result){                                                               // test 
								
								return test(clinicCode,true);
								
							},function(rejectResult){  
								
								var operation = 'Restore';
								return sendNotification(operation).
								then(function(result){
									console.log('exiting');
									process.exit();
								});
								
								
							}).then(function(result){                                                               // upload
								
								console.log('Verification complete');
								return upload();
								
							},function(rejectResult){
								
								var operation = 'Verification';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							}).then(function(result){
								console.log('all tasks completed.\n');
								return resolve();
								
							},function(rejectResult){
								var operation = 'Upload';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
							})
						}   
						else if(type === 'C' && objectToBackup[0].doc.hasOwnProperty('cms')){              // cms databases
							
							param_dbname = objectToBackup[0].doc.cms.cloudantDatabase.toString();
							param_password = objectToBackup[0].doc.cms.cloudantPassword.toString();
							param_username = objectToBackup[0].doc.cms.cloudantUsername.toString();
							
							var cmsBackupFileName = 'encrypted_'+param_dbname+'.txt';
							var cms_cmdCommand_backup = 'couchbackup --url https://'+ param_username + ':' + param_password
							+ '@vaultdragon.cloudant.com/ --db ' + param_dbname + ' | openssl aes-128-cbc -pass pass:vaultdragon' + ' > ' + cmsBackupFileName;
							
							console.log(cms_cmdCommand_backup);
							
							var promise = new Promise (function(resolve,reject){
								
								var backupFileName_enc = 'encrypted_'+param_dbname+'.txt';
								var cmdCommand_encrypt = 'openssl aes-128-cbc -e -in '+param_dbname+'.txt '+' -pass pass:vaultdragon -out '+backupFileName_enc;
								

								return resolve();
							/*
								return backup().
								then(function(result){
									cmd.get(cmdCommand_encrypt,function(err,data,stderr){
										if(!err){
											console.log('encryption complete');
											return resolve();
										}
										else{
											return reject();
										}
									})
								});
							*/	
								// cmd.get(cms_cmdCommand_backup,function(err,data,stderr){                    // complete backup
								// 	if(!err){
								// 		console.log('backup complete');
								// 		return resolve();
								// 	}    
								// 	else{
								// 		return reject();
								// 	}
								// });
								
							}).then(function(backupResult){
								
								var backupFileName_enc = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/encrypted_'+param_dbname+'.txt';
																					// complete decrypt + replace
								var backupFileName_dec = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt';
								var cmdCommand_decrypt = 'openssl aes-128-cbc -d -in '+backupFileName_enc +' -pass pass:vaultdragon -out '+backupFileName_dec;
								console.log(cmdCommand_decrypt);
								
								return decryptAndReplace(cmdCommand_decrypt,backupFileName_dec);
								
							}, function(rejectResult){
								
								var operation = 'Backup';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								})
							}).then(function(result){                                                       // complete restore
								
								var backupFileName = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt';
								var cmdCommand_pouch = 'pouchdb-server --port '+portNumber;
								
								return restore(cmdCommand_pouch,backupFileName,clinicCode,false);
								
							},function(rejectResult){
								
								var operation = 'Decrypt/Replace';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							}).then(function(result){                                                       // complete test
								
								return test(clinicCode, false);
								
							},function(rejectResult){
								
								var operation = 'Restore';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
							}).then(function(result){                                                       // complete upload
								
								console.log('Verification complete');
								return upload();
								
							},function(rejectResult){
								
								var operation = 'Verification';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							}).then(function(result){
								console.log('Upload Complete')
								console.log('all tasks completed\n');
								return resolve();
								
							},function(rejectResult){
								
								var operation = 'Upload';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							});
						}
						else if(type === 'A'){                  // other databases
							
							param_dbname = objectToBackup[0].doc.cloudantDatabase.toString();
							param_password = objectToBackup[0].doc.cloudantPassword.toString();
							param_username = objectToBackup[0].doc.cloudantUsername.toString();
							
							var backupFileName = 'encrypted_'+param_dbname+'.txt';
							var cmdCommand_backup = 'couchbackup --url https://'+ param_username + ':' + param_password
							+ '@vaultdragon.cloudant.com/ --db ' + param_dbname + ' | openssl aes-128-cbc -pass pass:vaultdragon' + ' > ' + backupFileName;
							
							console.log(cmdCommand_backup);
							
							var promise = new Promise (function(resolve,reject){
								var backupFileName_enc = 'encrypted_'+param_dbname+'.txt';
								var cmdCommand_encrypt = 'openssl aes-128-cbc -e -in '+param_dbname+'.txt '+' -pass pass:vaultdragon -out '+backupFileName_enc;
							

								return resolve();
							/*
								return backup().
								then(function(result){
									cmd.get(cmdCommand_encrypt,function(err,data,stderr){
										if(!err){
											console.log('encryption complete');
											return resolve();
										}
										else{
											return reject();
										}
									})
								});
							*/	
								// cmd.get(cmdCommand_backup,function(err,data,stderr){                        // complete backup
								// 	if(!err){
								// 		console.log('backup complete');
								// 		return resolve();
								// 	}    
								// 	else{
								// 		return reject();
								// 	}
								// });
							})
							.then(function(result){                                                      // complete decrypt + replace
								var backupFileName_enc = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/encrypted_'+param_dbname+'.txt';
								var backupFileName_dec = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt';
								var cmdCommand_decrypt = 'openssl aes-128-cbc -d -in '+backupFileName_enc +' -pass pass:vaultdragon -out '+backupFileName_dec;
								console.log(cmdCommand_decrypt);
								
								return decryptAndReplace(cmdCommand_decrypt,backupFileName_dec);
								
							},function(rejectResult){
								var operation = 'Backup';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							})
							.then(function(result){                                                      // complete restore
								
								var backupFileName = '/home/ubuntu/projects/backup-scripts/couchdb-backup-scripts/decrypted_'+param_dbname+'.txt';
								var cmdCommand_pouch = 'pouchdb-server --port '+portNumber;
								
								return restore(cmdCommand_pouch,backupFileName,clinicCode,false);
							},
							function(reject){
								
								var operation = 'Decrypt/Replace';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							})
							.then(function(result){                                                      // complete test
								return test(clinicCode, false);
							},function(reject){
								
								var operation = 'Restore';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							})
							.then(function(result){
								
								console.log('Verification complete');
								return upload();
								
							},function(rejectResult){
								var operation = 'Verification';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});                            
							})
							.then(function(result){
								
								console.log('Upload complete');
								console.log('all task completed\n');
								return resolve();
								
							},function(reject){
								
								var operation = 'Upload';
								return sendNotification(operation).
								then(function(result){
									process.exit();
								});
								
							});
						}
					}
				);
			});
			
		}
		// var request = rp("https://"+main_param_username+":"+main_param_password+"@vaultdragon.cloudant.com/"+main_param_dbname+"/_all_docs?include_docs=true")
		// .then(
		//     function(chunk){
		//         var object = JSON.parse(chunk.toString());
		//         var objectToBackup = _.filter(object.rows,function(o){
		//             return o.id === clinicCode;
		//         });
		//         if(type === 'D' && objectToBackup[0].doc.hasOwnProperty('dragonfly')){
		
		//             param_dbname = objectToBackup[0].doc.dragonfly.cloudantDatabase.toString();
		//             param_password = objectToBackup[0].doc.dragonfly.cloudantPassword.toString();
		//             param_username = objectToBackup[0].doc.dragonfly.cloudantUsername.toString();
		
		//             var dragonflyBackupFileName = 'encrypted_'+param_dbname+'.txt';
		
		//             var dragonfly_cmdCommand_backup = 'couchbackup --url https://'+ param_username + ':' + param_password
		//             + '@vaultdragon.cloudant.com/ --db ' + param_dbname + '| openssl aes-128-cbc -pass pass:vaultdragon' + ' > ' + dragonflyBackupFileName;
		
		//             console.log(dragonfly_cmdCommand_backup);
		
		//             var promise = new Promise (function(resolve,reject){
		
		//                 cmd.get(dragonfly_cmdCommand_backup,function(err,data,stderr){                              // complete backup
		//                     if(!err){
		//                         console.log('backup complete');
		//                         return resolve();                                                            
		//                     }    
		//                     else{
		//                         return reject();
		//                     }
		//                 });
		
		//             }).then(
		//                 function(backupResult){                                                                     // complete decrypt + replace
		
		//                     var backupFileName_enc = 'encrypted_'+param_dbname+'.txt';
		//                     var backupFileName_dec = 'decrypted_'+param_dbname+'.txt';
		//                     var cmdCommand_decrypt = 'openssl aes-128-cbc -d -in '+backupFileName_enc +' -pass pass:vaultdragon -out '+backupFileName_dec;
		//                     console.log(cmdCommand_decrypt);
		
		//                     return decryptAndReplace(cmdCommand_decrypt, backupFileName_dec);
		
		//                 },function(rejectResult){
		
		//                     var operation = 'backup';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){                                                                    // complete restore
		
		//                     var backupFileName = 'decrypted_'+param_dbname+'.txt';
		//                     var cmdCommand_pouch = 'pouchdb-server --port '+portNumber;
		
		//                     return restore(cmdCommand_pouch, backupFileName);
		
		//                 },function(rejectResult){
		
		//                     var operation = 'Decrypt/Replace';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){                                                               // test 
		
		//                     return test();
		
		//                 },function(rejectResult){  
		
		//                     var operation = 'Restore';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();  
		
		//                 }).then(function(result){                                                               // upload
		
		//                     console.log('Verification complete');
		//                     return upload();
		
		//                 },function(rejectResult){
		
		//                     var operation = 'Verification';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){
		//                     console.log('all tasks completed.\n');
		//                     process.exit();
		
		//                 },function(rejectResult){
		//                     var operation = 'Upload';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		//                 })
		//             }   
		//             else if(type === 'C' && objectToBackup[0].doc.hasOwnProperty('cms')){              // cms databases
		
		//                 param_dbname = objectToBackup[0].doc.cms.cloudantDatabase.toString();
		//                 param_password = objectToBackup[0].doc.cms.cloudantPassword.toString();
		//                 param_username = objectToBackup[0].doc.cms.cloudantUsername.toString();
		
		//                 var cmsBackupFileName = 'encrypted_'+param_dbname+'.txt';
		//                 var cms_cmdCommand_backup = 'couchbackup --url https://'+ param_username + ':' + param_password
		//                 + '@vaultdragon.cloudant.com/ --db ' + param_dbname + ' | openssl aes-128-cbc -pass pass:vaultdragon' + ' > ' + cmsBackupFileName;
		
		//                 console.log(cms_cmdCommand_backup);
		
		//                 var promise = new Promise (function(resolve,reject){
		//                     cmd.get(cms_cmdCommand_backup,function(err,data,stderr){                    // complete backup
		//                         if(!err){
		//                             console.log('backup complete');
		//                             return resolve();
		//                         }    
		//                         else{
		//                             return reject();
		//                         }
		//                     });
		
		//                 }).then(function(backupResult){
		
		//                     var backupFileName_enc = 'encrypted_'+param_dbname+'.txt';                  // complete decrypt + replace
		//                     var backupFileName_dec = 'decrypted_'+param_dbname+'.txt';
		//                     var cmdCommand_decrypt = 'openssl aes-128-cbc -d -in '+backupFileName_enc +' -pass pass:vaultdragon -out '+backupFileName_dec;
		//                     console.log(cmdCommand_decrypt);
		
		//                     return decryptAndReplace(cmdCommand_decrypt,backupFileName_dec);
		
		//                 }, function(rejectResult){
		
		//                     var operation = 'Backup';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){                                                       // complete restore
		
		//                     var backupFileName = 'decrypted_'+param_dbname+'.txt';
		//                     var cmdCommand_pouch = 'pouchdb-server --port '+portNumber;
		
		//                     return restore(cmdCommand_pouch,backupFileName);
		
		//                 },function(rejectResult){
		
		//                     var operation = 'Decrypt/Replace';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){                                                       // complete test
		
		//                     return test();
		
		//                 },function(rejectResult){
		
		//                     var operation = 'Restore';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){                                                       // complete upload
		
		//                     console.log('Verification complete');
		//                     return upload();
		
		//                 },function(rejectResult){
		
		//                     var operation = 'Verification';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){
		//                     console.log('all tasks completed\n');
		
		//                 },function(rejectResult){
		
		//                     var operation = 'Upload';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 });
		//             }
		//             else if(type === 'A'){                  // other databases
		
		//                 param_dbname = objectToBackup[0].doc.cloudantDatabase.toString();
		//                 param_password = objectToBackup[0].doc.cloudantPassword.toString();
		//                 param_username = objectToBackup[0].doc.cloudantUsername.toString();
		
		//                 var backupFileName = 'encrypted_'+param_dbname+'.txt';
		//                 var cmdCommand_backup = 'couchbackup --url https://'+ param_username + ':' + param_password
		//                 + '@vaultdragon.cloudant.com/ --db ' + param_dbname + ' | openssl aes-128-cbc -pass pass:vaultdragon' + ' > ' + backupFileName;
		
		//                 console.log(cmdCommand_backup);
		
		//                 var promise = new Promise (function(resolve,reject){
		//                     cmd.get(cmdCommand_backup,function(err,data,stderr){                        // complete backup
		//                         if(!err){
		//                             console.log('backup complete');
		//                             return resolve();
		//                         }    
		//                         else{
		//                             return reject();
		//                         }
		//                     });
		//                 }).then(function(result){                                                      // complete decrypt + replace
		
		//                     var backupFileName_enc = 'encrypted_'+param_dbname+'.txt';
		//                     var backupFileName_dec = 'decrypted_'+param_dbname+'.txt';
		//                     var cmdCommand_decrypt = 'openssl aes-128-cbc -d -in '+backupFileName_enc +' -pass pass:vaultdragon -out '+backupFileName_dec;
		//                     console.log(cmdCommand_decrypt);
		
		//                     return decryptAndReplace(cmdCommand_decrypt,backupFileName_dec);
		
		//                 },function(rejectResult){
		//                     var operation = 'Backup';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		
		//                     process.exit();
		
		//                 }).then(function(result){                                                      // complete restore
		
		//                     var backupFileName = 'decrypted_'+param_dbname+'.txt';
		//                     var cmdCommand_pouch = 'pouchdb-server --port '+portNumber;
		
		//                     return restore(cmdCommand_pouch,backupFileName);
		
		
		//                 },function(reject){
		
		//                     var operation = 'Decrypt/Replace';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		//                     process.exit();
		
		//                 }).then(function(resolve){                                                      // complete test
		
		//                     return test();
		
		//                 },
		//                 function(reject){
		
		//                     var operation = 'Restore';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		//                     process.exit();
		
		//                 })
		//                 .then(function(resolve){
		
		//                     console.log('Verification complete');
		//                     return upload();
		
		//                 },function(reject){
		
		//                     var operation = 'Verification';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		//                     process.exit();
		
		//                 })
		//                 .then(function(result){
		
		//                     console.log('Upload complete');
		//                     console.log('all task completed\n');
		
		//                 },function(rejectResult){
		//                     var operation = 'Upload';
		//                     return new Promise ((resolve,reject) => {
		//                         sendNotification(operation);
		//                         return resolve();
		//                     })
		//                     process.exit();
		
		//                 });
		//             }
		//         }
		//     );
		
		// });
		
		
